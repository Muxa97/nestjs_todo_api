import * as mongoose from 'mongoose';

export const TodoSchema = new mongoose.Schema({
  text: { type: String, required: true },
  createdAt: { type: Date, default: Date.now },
  done: { type: Boolean, default: false }
});

TodoSchema.index({ text: 'text' });