import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { Todo } from './interfaces/todo.interface';
import { TodoDto, CreateTodoDto, UpdateTodoDto, DeleteTodoDto } from './dto/create-todo.dto';
import { TODO_MODEL } from './constants';

@Injectable()
export class TodosService {
  constructor(
    @Inject(TODO_MODEL)
    private todoModel: Model<Todo>,
  ) {}

  async create(body: CreateTodoDto): Promise<TodoDto> {
    const createdTodo = new this.todoModel(body);
    return createdTodo.save();
  }

  async findAll(): Promise<TodoDto[]> {
    const todos = await this.todoModel.find().sort('createdAt').exec();
    return todos.map(todo => todo as TodoDto );
  }

  async searchTodos(text: string): Promise<TodoDto[]> {
    const todos = await this.todoModel.find({
      $text: { $search: text }
    }).sort('createdAt').exec();
    return todos.map(todo => todo as TodoDto);
  }

  async completeTodo(updateTodoDto: UpdateTodoDto): Promise<boolean> {
    try {
      const res = await this.todoModel.updateOne(
        { _id: updateTodoDto._id },
        { done: updateTodoDto.done }
        ).exec();
      return res.nModified === 1;
    } catch (err) {
      return false;
    }
  }

  async deleteTodo(deleteTodoDto: DeleteTodoDto): Promise<boolean> {
    try {
      const res = await this.todoModel.deleteOne({ _id: deleteTodoDto._id }).exec();
      return res.deletedCount === 1;
    } catch (err) {
      return false;
    }
  }
}