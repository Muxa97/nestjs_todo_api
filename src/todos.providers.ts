import { Connection } from 'mongoose';
import { TodoSchema } from './schemas/todo.schema';
import { TODO_MODEL, DB_CONNECTION } from './constants';

export const todosProvider = [{
  provide: TODO_MODEL,
  useFactory: (connection: Connection) => connection.model('Todo', TodoSchema),
  inject: [ DB_CONNECTION ] 
}];