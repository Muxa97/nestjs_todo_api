import { Controller, Body, Query, Get, Post, Delete } from '@nestjs/common';
import { TodosService } from './todos.service';
import { TodoDto, CreateTodoDto, UpdateTodoDto, DeleteTodoDto } from './dto/create-todo.dto';
import { ApiResponse, ApiBody, ApiTags } from '@nestjs/swagger';

@ApiTags("todos")
@Controller('todos')
export class TodosController {
  constructor(private readonly todosService: TodosService) {}

  @ApiResponse({ status: 200, description: 'An array of all todos from database', type: [TodoDto] })
  @Get()
  getAll(): Promise<TodoDto[]> {
    return this.todosService.findAll();
  }

  @ApiResponse({ status: 200, description: 'An array of all todos containing search text', type: [TodoDto] })
  @Get('search')
  searchTodos(@Query('text') text: string): Promise<TodoDto[]> {
    return this.todosService.searchTodos(text);
  }

  @ApiResponse({ status: 200, description: 'Created todo', type: TodoDto })
  @ApiBody({ type: CreateTodoDto })
  @Post()
  createTodo(@Body() body: { text: string }): Promise<TodoDto> {
    return this.todosService.create(body);
  }

  @ApiResponse({ status: 200, description: "Returns todo id with update result"})
  @ApiBody({ type: UpdateTodoDto })
  @Post('done')
  async completeTodo(
    @Body() updateTodoDto: UpdateTodoDto
  ): Promise<{ _id: string, successful: boolean }> {
    const result = await this.todosService.completeTodo(updateTodoDto);
    return {
      _id: updateTodoDto._id,
      successful: result
    };
  }

  @ApiResponse({ status: 200, description: "Delete todo with given id", type: Boolean})
  @ApiBody({ type: DeleteTodoDto })
  @Delete()
  deleteTodo(@Body() deleteTodoDto: DeleteTodoDto): Promise<boolean> {
    return this.todosService.deleteTodo(deleteTodoDto);
  }
}
