import { ApiProperty } from '@nestjs/swagger';

export class CreateTodoDto {
  @ApiProperty()
  text: string;
}

export class UpdateTodoDto {
  @ApiProperty()
  _id: string;

  @ApiProperty()
  done: boolean;
}

export class DeleteTodoDto {
  @ApiProperty()
  _id: string;
}

export class TodoDto {
  @ApiProperty()
  _id: string;

  @ApiProperty()
  text: string;

  @ApiProperty()
  done: boolean;
}