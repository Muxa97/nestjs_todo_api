import * as mongoose from 'mongoose';
import { DB_CONNECTION } from './constants';

export const databaseProviders = [
  {
    provide: DB_CONNECTION,
    useFactory: (): Promise<typeof mongoose> =>
      mongoose.connect('mongodb://localhost:27017/todo_test'),
  },
];