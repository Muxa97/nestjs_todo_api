import { NestFactory } from '@nestjs/core';
import { TodosModule } from './todos.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(TodosModule);

  const options = new DocumentBuilder()
    .setTitle('Test TODO API')
    .setDescription('API for todo list application')
    .setVersion('1.0')
    .addTag('todos')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);

  await app.listen(3000);
}
bootstrap();
