import { Test } from '@nestjs/testing';
import { TodosController } from './todos.controller';
import { TodosService } from './todos.service';
import { DatabaseModule } from './database.module';
import { todosProvider} from './todos.providers';

describe("Todos controller", () => {
  let todosController: TodosController;
  let todosService: TodosService;
  const todos = [
    { text: '11 11', _id: '0', done: false },
    { text: '11 test', _id: '1', done: false },
    { text: '11 11', _id: '2', done: false },
    { text: 'test 11', _id: '3', done: true }
  ];
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [DatabaseModule],
      controllers: [TodosController],
      providers: [TodosService, ...todosProvider ],
    }).compile();

    todosService = moduleRef.get<TodosService>(TodosService);
    todosController = moduleRef.get<TodosController>(TodosController);
  });

  describe("searchTodos", () => {
    it("should return todos that contain word 'test'", async () => {
      const searchingValue = 'test';
      const res = todos.filter(todo => todo.text.includes(searchingValue));
      jest.spyOn(todosService, 'searchTodos').mockImplementation(async () => res);

      expect(await todosController.searchTodos(searchingValue)).toEqual(res);
    });
  });
});